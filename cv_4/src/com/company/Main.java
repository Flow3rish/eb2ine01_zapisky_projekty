package com.company;

// import an existing library //
import java.util.*;

public class Main {

    public static void main(String[] args) {

        int width = 5;
        int height = 5;

        for (int i = 0; i < height; ++i){
            if (i == 0 || i == height - 1) {
                // full row
                for (int j = 0; j < width; ++j) {
                System.out.print("*\t");
            }
            System.out.print('\n');
            } else {
               // first and last element filled with spaces
                for (int j = 0; j < width; ++j) {
                    if(j == 0 || j == width - 1) {
                        System.out.print("*\t");
                    }
                    else {
                        System.out.print("\t");
                    }
                 }
                // a new layer for the rectangle
                System.out.print('\n');
            }
        }

        int array1[] = new int[2];
        array1[0] = 10;
        array1[1] = 15;

        int array2[] = new int[2];
        array2[0] = 10;
        array2[1] = 15;
        // calls an array comparison method from the utils library
        System.out.print("pole stejna: " + Arrays.equals(array1, array2) + "\n");

        int array3[] = new int[50];
        // calls an array filling method from the utils library
        Arrays.fill(array3, 0);
        // calls overloaded filling method from the utils library
        Arrays.fill(array3, 9, 19, 5);

        // prints our array3
        for (int i = 0; i < array3.length; ++i) {
            if (i % 10 == 0) {                          // for clean formatting
                System.out.print('\n');
            }
            System.out.print(array3[i] + " ");
        }

        String array4[] = new String[3];
        array4[0] = "foo";
        array4[1] = "bar";
        array4[2] = "boo";
        Arrays.sort(array4);

        for (int i = 0; i < array4.length; ++i) {
            if (i % 10 == 0) {                          // for clean formatting
                System.out.print('\n');
            }
            System.out.print(array4[i] + " ");
        }

        System.out.print('\n');


        // methods //
        int a = 10;
        int b = 14;
        System.out.print("Soucet cisel " + a + " a " + b + " je " + sum(a, b) + '\n');
        System.out.print("Pomer cisel " + a + " a " + b + " je " + ratio(a, b) + '\n'); // a and b are integers, they are converted to doubles,
                                                                                        // since the method takes arguments of type double
        System.out.print("factorial: " + factorial(4));
    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static double ratio(double a, double b) {
        return a/b;
    }

    public static int factorial(int a) {
        if(a == 0){
            return 1;
        } else {
            return a * factorial(a - 1);
        }
    }






}
