package com.company;

public class Main {

    public static void main(String[] args) {

        int i = 10;
        i = i + 1; // 11
        i++; // 12 evaluate and increment
        ++i; // 13 increment and evaluate

        i = i - 1; // 12
        i--; // 11
        --i; // 10

        i += 5; // 15

        i *= 10; // 150


            System.out.print("Modulo: " + 15 % 6 + "\nCislo " + i++ + '\n'); // 150
            System.out.print("Modulo: " + 15 % 6 + "\nCislo " + ++i + '\n'); // 152

        if(i > 0) {
            System.out.print("i je vetsi nez 0\n");
        }
        else {
            System.out.print("i neni vetsi nez 0\n");
        }

        // kontrola sudosti/lichosti //

        if(i % 2 ==  0) {
            System.out.print("i je sude\n");
        }
        else {
            System.out.print("i je liche\n");
        }

        int prvni = 1;
        int druhe = 4;
        int treti = 3;

        if(prvni > treti && prvni > druhe) {
            // prvni je nejvetsi
            System.out.print("1\n");
        }
        if(druhe > prvni && druhe > treti) {
            // druhe je nejvetsi
            System.out.print("2\n");
        }
        if(treti > prvni && treti > druhe) {
            // treti je nejvetsi
            System.out.print("3\n");
        }
        else {
            System.out.print("dve nebo vice nejvetsich cisel jsou si rovny\n");
        }

        int switch_this = 20;
        switch(switch_this) {
            default: {
                System.out.print("other\n");
                break;
            }
            case 1 : {
                System.out.print("1\n");
                break;
            }
            case 2 : {
                System.out.print("2\n");
                break;
            }
            case 3 : {
                System.out.print("3\n");
                break;
            }
        }



    }
}
