package cz.vsb.ekf.ai;

public class UserOrder {
    // run the program from here //
    public static void main(String[] args) {
        int shipyard_capacity = 10;
        // initialize the ships available at shipyard //
        int list_length = 5;
        Ship[] ship_list = new Ship[list_length];
        ship_list[0] = new Ship("Black Pearl", 100000.0);
        ship_list[1] = new Ship("Millenium Falcon", 1890000.0);
        ship_list[2] = new Ship("Enterprise", 100000000.0);
        ship_list[3] = new Ship("Tardis", 530000000.2);
        ship_list[4] = new Ship("Red Dwarf", 9800.0);
        Shipyard my_shipyard = new Shipyard(ship_list, shipyard_capacity);


        // take an order from user //
        ShipyardOrder order = new ShipyardOrder(my_shipyard);

        // end of the program, reviews the order //
        order.printOrder();
    }
}
