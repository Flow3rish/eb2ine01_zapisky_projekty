package cz.vsb.ekf.ai;

public class Ship {
    private String m_shipType;
    private int m_quantity;
    private double m_unit_price;

    // constructors //
    public Ship(String m_shipType, int m_quantity, double m_unit_price) {
        this.m_shipType = m_shipType;
        this.m_quantity = m_quantity;
        this.m_unit_price = m_unit_price;
    }
    public Ship(String m_shipType, double m_unit_price) {
        this.m_shipType = m_shipType;
        this.m_unit_price = m_unit_price;
    }

    // getters and setters //
    public void setM_shipType(String m_shipType) {
        this.m_shipType = m_shipType;
    }
    public String getM_shipType() {
        return m_shipType;
    }
    public int getM_quantity() {
        return m_quantity;
    }
    public void setM_quantity(int m_quantity) {
        this.m_quantity = m_quantity;
    }
    public double getM_unit_price() {
        return m_unit_price;
    }
    public void setM_unit_price(double m_unit_price) {
        this.m_unit_price = m_unit_price;
    }
}


