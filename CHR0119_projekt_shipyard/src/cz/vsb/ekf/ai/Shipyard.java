package cz.vsb.ekf.ai;

import java.util.ArrayList;
import java.util.*;

public class Shipyard {

    private Ship[] m_shipList;
    private int m_shipyard_capacity; // how many ships the shipyard can make

    // constructors //
    public Shipyard(Ship[] m_shipList, int m_shipyard_capacity) {
        this.m_shipList = m_shipList;
        this.m_shipyard_capacity = m_shipyard_capacity;
    }

    // getters and setters //
    public Ship[] getM_shipList() {
        return m_shipList;
    }
    public int getM_shipyard_capacity() { return m_shipyard_capacity; }

    // custom functions //
    public void printList() {
        for (Ship s : m_shipList) {
            System.out.print(s.getM_shipType() + ": " + s.getM_unit_price() + "\n");
        }
    }
    public boolean isAvailable(String shipType) {
        for (Ship availship : m_shipList) {
            if (shipType.equals(availship.getM_shipType())) {
                return true;
            }
        }
        return false;
    }
    public double searchUnitPrice(String shipType) {
        for (Ship availship : m_shipList) {
            if (shipType.equals(availship.getM_shipType())) {
                return availship.getM_unit_price();
            }
        }
        return 0.0;
    }


}

class ShipyardOrder {
    private static int s_orders_total = 0;
    private ArrayList<Ship> m_orderList;
    private int m_order_total;

    // constructors //
    public ShipyardOrder(Shipyard shipyard) { // ShipyardOrder object depends on the existence of Shipyard object
        m_orderList = new ArrayList<Ship>();
        m_order_total = 0;
        this.takeOrder(shipyard);
    }

    // getters and setters //
    public static int getS_orders_total() {
        return s_orders_total;
    }
    public ArrayList<Ship> getM_orderList() {
        return m_orderList;
    }
    public void setM_orderList(ArrayList<Ship> m_orderList) {
        this.m_orderList = m_orderList;
    }
    public int getM_order_total() {
        return m_order_total;
    }

    // custom functions //
    private void takeOrder(Shipyard my_shipyard) { // depends on existing Shipyard object
        // list available items
        System.out.print("Možno vyrobit: cena\n");
        my_shipyard.printList();
        System.out.print("\n");

        // set up a simple scanner to take input from user
        Scanner scanner = new Scanner(System.in);
        String scanned_String;
        int scanned_quantity = 0;
        double scanned_unit_price = 0;
        boolean found = false;
        boolean invalid_type = false;

        while (this.getM_order_total() < my_shipyard.getM_shipyard_capacity()) {

            // input type and search unit price
            System.out.print(ShipyardOrder.getS_orders_total() + 1 + ". Zadejte typ lodě k výrobě: ");
            scanned_String = scanner.nextLine();

            found = my_shipyard.isAvailable(scanned_String);

            if (!found) {
                System.out.print("Tento typ lodě nevyrábíme\n");
                continue;
            }

            scanned_unit_price = my_shipyard.searchUnitPrice(scanned_String);

            // input quantity
            invalid_type = false;
            do {
                try {
                    System.out.print("Zadejte množství lodě " + scanned_String + ": ");
                    scanned_quantity = scanner.nextInt();
                    invalid_type = false;
                } catch (Exception e) {
                    System.out.print("Chybný datový typ\n");
                    invalid_type = true;
                    scanner.nextLine();
                } finally {
                    if (scanned_quantity <= 0 && !invalid_type) {
                        System.out.print("Špatná hodnota\n");
                        invalid_type = true;
                    }
                }
            } while (invalid_type);

            // adds created entry
            this.addItem(new Ship(scanned_String, scanned_quantity, scanned_unit_price));
            scanner.nextLine();

        }
    }

    public void printOrder() {
        for (Ship s : m_orderList) {
            System.out.print("\nBude vyrobeno\n");
            System.out.print(s.getM_quantity() + "x " + s.getM_shipType()
                    + " za " + s.getM_unit_price()*s.getM_quantity() + "\n");
        }
    }

    public void addItem(Ship ship) {
        m_orderList.add(ship);
        m_order_total += ship.getM_quantity();
        ++s_orders_total;
    }

}

