package cz.vsb.ekf.ai;

public class Prumer {
    protected double m_prumer;

    Prumer() {
        m_prumer = 0;
    }


    public double getPrumer() { return m_prumer; }

    public void vypoctiPrumer(double x, double y) {
        m_prumer = (x+y)/2;
        System.out.print("vypocteny prumer:" + m_prumer + "\n");

    }

    public final void printPrumer(double prumer){
        System.out.print("prumer:" + prumer + "\n");
    }
}

