package cz.vsb.ekf.ai;

public class PrumerPole extends Prumer {


    public void vypoctiPrumer(double pole[]) {
        double sum = 0;
        int count = 0;
        for (int i = 0; i < pole.length; ++i) {
            sum += pole[i];
            count += 1;
        }

        m_prumer = sum/count;
        System.out.print("vypocteny prumer" + m_prumer + "\n");
    }
}
