package cz.vsb.ekf.ai;

// napsal CHR0119



public class Main {

    public static void main(String[] args) {
        double x = 2.7;
        double y = 3.3;

        int length = 4;
        double[] array = new double[length];
        array[0] = 5.0;
        array[1] = 3.9;
        array[2] = 5.8;
        array[3] = 9.1;

        Prumer prumer = new Prumer();
        PrumerPole prumer_pole = new PrumerPole();


        prumer.vypoctiPrumer(x, y);
        prumer_pole.vypoctiPrumer(array);


        prumer.printPrumer(prumer.getPrumer());
        prumer_pole.printPrumer(prumer_pole.getPrumer());
    }
}
