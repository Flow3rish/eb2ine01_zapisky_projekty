package cz.vsb.ekf.ai;

interface Zvire {
    public void sezer();
    public void bez();
}

interface VelkeZvire extends Zvire {
    public void zaval();
}

interface Plaz {
    public void plazSe();
    public void zasyc();
}

class Tucnak implements Zvire {

    @Override
    public void sezer() {
        System.out.print("sezral te tucnak\n");
    }

    @Override
    public void bez() {
        System.out.print("tucnak plachl\n");
    }
}

class Krokodyl implements VelkeZvire, Plaz {
    @Override
    public void sezer() {
        System.out.print("krokodyl te sezral\n");
    }

    @Override
    public void bez() {
        System.out.print("krokodyl utekl\n");
    }

    @Override
    public void zaval() {
        System.out.print("krokodyl te zavalil\n");
    }

    @Override
    public void plazSe() {
        System.out.print("krokodyl te plazi\n");
    }

    @Override
    public void zasyc() {
        System.out.print("krokodyl zasyci\n");
    }
}

public class Main {

    public static void main(String[] args) {
	// write your code here
        Tucnak t = new Tucnak();
        t.bez();
        t.sezer();

        Krokodyl k = new Krokodyl();
        k.plazSe();
        k.zasyc();
        k.sezer();
    }
}
