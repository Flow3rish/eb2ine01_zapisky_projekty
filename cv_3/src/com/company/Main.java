package com.company;

public class Main {

    public static void main(String[] args) {
        // for loop //
        int condition = 10;
        for (int i = 1; i < condition + 1; ++i) {
            // do code
            System.out.print(i + " ");
            if (i == 5) {
                System.out.print("je to pet a ");
            }
            if (i % 2 == 0) {
                System.out.print("je to sude \n");
            } else {
                System.out.print("je to liche \n");
            }
        }

        for (int i = 0; i < 10; ++i) {
            System.out.print("i " + i + '\n');


            for (int j = 0; j < 10; ++j) {
                System.out.print("j " + j + '\n');
                if (i == j) {
                    System.out.print("i " + i + " = " + j + " j" + '\n');
                }
            }
        }

        // while loop //
        int x = 0;
        while(true) {
           ++x;
           if(x == 3) {
                break;
            }
        }
        int y = 0;

        // do while loop //
        do {
            ++y;
            if(y == 10) {
                break;
            }
        }
        while(true);

        // array //
        int array[];
        int length = 10;
        array = new int[length];
        array[0] = 1;
        for (int index = 0; index < length; ++index) {
            array[index] = index + 1;
        }

        for (int index = 0; index < length; ++index) {
            System.out.print(array[index] + " ");
        }
        System.out.print('\n');


        // vytvor prazdne dvourozmerne //
        int array_2[][];
        array_2 = new int[length][length];

        // vypln matici //
        for(int j = 0; j < length; ++j) {
            for (int i = 0; i < length; ++i) {
                array_2[i][j] = (i + 1) * (j + 1);
            }
        }

        // vytiskni matici //
        for(int j = 0; j < length; ++j) {
            for (int i = 0; i < length; ++i) {
                System.out.print(array_2[i][j] + "\t");
            }
            System.out.print("\n");
        }


        }
    }
