package cz.vsb.ekf.ai;

import java.util.*;

public class Seznam {
    private List<Student> seznam;

    public Seznam() {
        seznam = new ArrayList<>();
    }

    public void pridat(Student student) {
        seznam.add(student);
    }

    public void odebrat(Student student) {
        seznam.remove(student);
    }

    public void vytiskni() {
        for (Student student : seznam) {
            System.out.print(student + "\t");
        }
        System.out.print("\n");
    }
}

