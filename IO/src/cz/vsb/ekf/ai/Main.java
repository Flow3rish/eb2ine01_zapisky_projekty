package cz.vsb.ekf.ai;

import java.util.*;


public class Main {

    public Main() {
        System.out.print("vytvarim objekt main.\n");
    }


    public void printStrAndInt(String your_string, int your_int) {
        System.out.print("vase slovo: " + your_string + "\n" + "Vase cislo: " + your_int + "\n");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Vlozte slovo: ");
        String string_text = scanner.next();
        System.out.print("Vlozeno slovo " + string_text + "\n");

        System.out.print("Vlozte integer: ");
        int int_text = scanner.nextInt();
        System.out.print("Vlozen integer: " + int_text + "\n");

        // since the method is not static, an object must be created to call it
        Main io = new Main();
        new Main().printStrAndInt(string_text, int_text); // we can solve this by creating an anonymous object
        io.printStrAndInt(string_text, int_text);



        boolean option = true;
        Seznam seznam = new Seznam();
        while (option) {
            System.out.print("Zadej jmeno studenta: ");
            String jmeno = scanner.next();

            System.out.print("Zadej znamku studenta: ");
            int znamka = scanner.nextInt();

            System.out.print("Zadej komentar ke studentovi: ");
            String komentar = scanner.next();
            seznam.pridat(new Student(jmeno, znamka, komentar));

            System.out.print("chcete pridat dalsiho studenta? ");
            option = scanner.nextBoolean();
        }

        seznam.vytiskni();
    }
}
