package cz.vsb.ekf.ai;

public class Student {
    private String m_name;
    private int m_grade;
    private String m_comment;

    public Student(String name, int grade, String comment) {
        this.m_name = name;
        this.m_grade = grade;
        this.m_comment = comment;
    }

    public String toString() {
        return m_name + " " + m_grade + " " + m_comment;
    }
}
