package cz.vsb.ekf.ai;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List list = new ArrayList();
        List copylist = new ArrayList();


        list.add("hello");
        list.add("world");
        list.add("foo");
        list.add("bar");
        list.add(1, "boo");
        //list.add(null); // can't sort

        copylist.add("Emma");
        copylist.add("Sarah");
        copylist.add("bar");
        copylist.add("Eve");

        //list.addAll(copylist);
        //list.retainAll(copylist); // prunik

        //for (Object element : list) {
        //    System.out.println(element);
        //}

        List<String> string_list = new ArrayList<>();
        string_list.addAll(list);
        String[] string_array = string_list.toArray(new String[0]);

        List sublist = list.subList(0, 3); // sublist from index 0 to index 3

        Collections.sort(list); // calls a static method from class collection to sort a list

        for (Object element : list) {
            System.out.println(element);
        }
        System.out.print("\n\n");

        Scanner scanner = new Scanner(System.in);
        ArrayList<String> input_list = new ArrayList<>();
        String scanned = "";
        for (int i = 0; i < 5; ++i) {
            System.out.print(i + 1 + ": Zadejte slovo: ");
            scanned = scanner.next();
            input_list.add(scanned);
            scanner.nextLine();
        }
        Collections.sort(input_list);
        for (String element : input_list) {
            System.out.println(element);
        }



    }
}
